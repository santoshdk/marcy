<div class="sidebar" id="sidebar">
    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <i class="icon-signal"></i>
            </button>

            <button class="btn btn-info">
                <i class="icon-pencil"></i>
            </button>

            <button class="btn btn-warning">
                <i class="icon-group"></i>
            </button>

            <button class="btn btn-danger">
                <i class="icon-cogs"></i>
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- #sidebar-shortcuts -->

    <ul class="nav nav-list">

        <li class="{{ Request::is('dashboard')?'active':'' }}">
            <a href="{{ route('admin.dashboard') }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> {{ trans('admin/dashboard/general.dashboard') }} </span>
            </a>
        </li>

        <li class="{{ Request::is('profile')?'active':'' }}">
            <a href="{{ route('admin.profile') }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Profile </span>
            </a>
        </li>

        <li class="{{ Request::is('principle')?'active':'' }}">
            <a href="{{ route('admin.principle.add') }}">
                <i class="icon-double-angle-right"></i>
                Principle
            </a>
        </li>

        <li {!! Request::is('feedback*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> Feedback</span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li {!! Request::is('feedback')?'class="active"':"" !!}>
                    <a href="{{ route('admin.feedback.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! Request::is('feedback/add')?'class="active open"':"" !!}>
                    <a href="{{ route('admin.feedback.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! Request::is('feature*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> Feature </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li {!! Request::is('feature')?'class="active"':"" !!}>
                    <a href="{{ route('admin.feature.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! Request::is('feature/add')?'class="active open"':"" !!}>
                    <a href="{{ route('admin.feature.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! Request::is('about-us*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> About Us </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li {!! Request::is('about-us')?'class="active"':"" !!}>
                    <a href="{{ route('admin.about-us.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! Request::is('about-us/add')?'class="active open"':"" !!}>
                    <a href="{{ route('admin.about-us.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! Request::is('information*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-info"></i>
                <span class="menu-text"> Information </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li {!! Request::is('information')?'class="active"':"" !!}>
                    <a href="{{ route('admin.information.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! Request::is('information/add')?'class="active open"':"" !!}>
                    <a href="{{ route('admin.information.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! Request::is('club-and-eca*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-info"></i>
                <span class="menu-text"> Club and Eca </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li {!! Request::is('club-and-eca')?'class="active"':"" !!}>
                    <a href="{{ route('admin.club-and-eca.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! Request::is('club-and-eca/add')?'class="active open"':"" !!}>
                    <a href="{{ route('admin.club-and-eca.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! Request::is('news*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> News </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li {!! Request::is('news')?'class="active"':"" !!}>
                    <a href="{{ route('admin.news.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! Request::is('news/add')?'class="active open"':"" !!}>
                    <a href="{{ route('admin.news.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! Request::is('banner*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> Banner </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li {!! Request::is('banner')?'class="active"':"" !!}>
                    <a href="{{ route('admin.banner.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! Request::is('banner/add')?'class="active open"':"" !!}>
                    <a href="{{ route('admin.banner.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! Request::is('gallery*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-camera"></i>
                <span class="menu-text"> Gallery </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li {!! Request::is('gallery')?'class="active"':"" !!}>
                    <a href="{{ route('admin.gallery.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! Request::is('gallery/add')?'class="active open"':"" !!}>
                    <a href="{{ route('admin.gallery.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! Request::is('photo*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-camera"></i>
                <span class="menu-text"> Photo </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li {!! Request::is('photo')?'class="active"':"" !!}>
                    <a href="{{ route('admin.photo.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! Request::is('photo/add')?'class="active open"':"" !!}>
                    <a href="{{ route('admin.photo.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! Request::is('menu*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> Menu </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li {!! Request::is('menu')?'class="active"':"" !!}>
                    <a href="{{ route('admin.menu.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! Request::is('menu/add')?'class="active open"':"" !!}>
                    <a href="{{ route('admin.menu.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! Request::is('page*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> Page </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li {!! Request::is('page')?'class="active"':"" !!}>
                    <a href="{{ route('admin.page.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! Request::is('page/add')?'class="active open"':"" !!}>
                    <a href="{{ route('admin.page.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! Request::is('sub-menu*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> Sub Menu </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li {!! Request::is('sub-menu')?'class="active"':"" !!}>
                    <a href="{{ route('admin.sub-menu.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! Request::is('sub-menu/add')?'class="active open"':"" !!}>
                    <a href="{{ route('admin.sub-menu.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

    </ul><!-- /.nav-list -->

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
    </script>
</div>