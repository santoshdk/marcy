@extends('admin.common.layout.layout')

@section('page_title'){{ $view_title }} - Admin Panel @endsection

@section('content')


    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            @include($view_path.'.partials.breadcrumb')

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off">
									<i class="icon-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ $view_title }}
                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans($trans_path.'content.list.list') }}
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">

                                @if (Request::session()->has('message'))
                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="icon-remove"></i>
                                        </button>
                                        {!! Request::session()->get('message') !!}
                                        <br>
                                    </div>
                                @endif

                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">
                                            SN
                                        </th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Contact</th>
                                       <th>Email</th>
                                       <th>Status</th>
                                       <th>Added By</th>
                                       <th>Updated By</th>

                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    @if ($data['rows']->count() > 0)
                                        @php $counter=0; @endphp
                                        @foreach($data['rows'] as $row)
                                            @php $counter++ @endphp

                                            <tr>
                                                <td class="center">
                                                    {{ $counter }}
                                                </td>

                                                <td>
                                                    {{ $row->name }}
                                                </td>
                                                <td>
                                                    {{ $row->address }}
                                                </td>

                                                <td>{{ $row->contact }}</td>
                                                <td>{!!  $row->email  !!}</td>
                                                <td class="hidden-480">
                                                    @if ($row->status == 1)
                                                        <span class="label label-sm label-success">Active</span>
                                                    @else
                                                        <span class="label label-sm label-info">In-Active</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    {!! MyHelper::createdby($row->created_by) !!}
                                                </td>
                                                <td>
                                                {!! MyHelper::updatedby($row->updated_by) !!}
                                                <td>
                                                    <div class="btn-group">

                                                        <a class="btn btn-xs btn-info" href="{{ route($base_route.'.edit', ['id' => $row->id]) }}">
                                                            <i class="icon-edit bigger-120"></i>
                                                        </a>

                                                        <!--<a class="btn btn-xs btn-danger bootbox-confirm" href="{{ route($base_route.'.delete', ['id' => $row->id]) }}">
                                                            <i class="icon-trash bigger-120"></i>
                                                        </a>-->

                                                    </div>


                                                </td>
                                            </tr>

                                            @endforeach

                                        @else

                                        <tr>
                                            <td colspan="7">No data found.</td>
                                        </tr>

                                        @endif

                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-4">
                        {{ $data['rows']->links() }}
                            </div>

                            <!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

    @endsection

@section('page_specific_scripts')

    <script src="{{ asset('assets/admin/js/bootbox.min.js') }}"></script>
    <script>
        $(".bootbox-confirm").on(ace.click_event, function() {
            console.log('sdfd');
            bootbox.confirm("Are you sure?", function(result) {
                if(result) {
                    //
                }
            });
        });
    </script>


@endsection