<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>

<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="title"> Title </label>

    <div class="col-sm-9">
        <input type="text" name="title" id="title" value="{{ ViewHelper::getData('title', isset($data['santosh'])?$data['santosh']:[]) }}" placeholder="Title" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="description"> Description </label>

    <div class="col-sm-9">
        <textarea name="description" id="description" placeholder="Description" class="col-xs-10 col-sm-5">{{ ViewHelper::getData('description', isset($data['santosh'])?$data['santosh']:[]) }}</textarea>
        <script>
            CKEDITOR.replace( 'description', {
                filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
            });
        </script>
    </div>
</div>
<div class="space-4"></div>

<div class="form-group" id="parent_menu_hide_gar">
    <label class="col-sm-3 control-label no-padding-right" for="parent_menu"> Page Link </label>

    <div class="col-sm-9">
        <select name="page_link" id="parent_menu" class="col-xs-10 col-sm-5">
            <option value="gallery" {!! (isset($data['row']->page_link) && $data['row']->page_link=='gallery')?'selected':'' !!}>Gallery</option>
            <option value="about" {!! (isset($data['row']->page_link) && $data['row']->page_link=='about')?'selected':'' !!}>About Us</option>
            <option value="contact" {!! (isset($data['row']->page_link) && $data['row']->page_link=='contact')?'selected':'' !!}>Contact Us</option>
            <option value="newss" {!! (isset($data['row']->page_link) && $data['row']->page_link=='newss')?'selected':'' !!}>News</option>
            <option value="simplepage" {!! (isset($data['row']->page_link) && $data['row']->page_link=='simplepage')?'selected':'' !!}>Simple Page</option>
        </select>

    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="status"> Status </label>

    <div class="col-sm-9">
        <div class="radio">
            <label>
                <input name="status" value="1" checked type="radio" class="ace">
                <span class="lbl"> Active</span>
            </label>
        </div>
        <div class="radio">
            <label>
                <input name="status" value="0" type="radio" class="ace">
                <span class="lbl"> In-active</span>
            </label>
        </div>
    </div>
</div>
<div class="space-4"></div>

@if(isset($data['row']))

<script>
    
    $(document).ready(function () {
        $('#delete_image_aboutus').click(function () {
            $.ajax({
                type : 'POST',
                url : '{{ route('admin.image.about-us.delete') }}',
                data : {
                    _token : '{{ csrf_token() }}',
                    id : {{ $data['row']->id }}
                },
                success : function (response) {
                    var data = $.parseJSON(response);

                    if (data.error){

                    }else{
                        alert('delete successful');
                        $('#aboutus_image_div').hide();
                        $('#old_image_val').val(null);
                    }

                },
                error : {

                }
            });
        });
    });

</script>

    @endif