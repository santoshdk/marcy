<script type="text/javascript" src="{{ asset('node_modules/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<div class="about" id="about">
    <div class="container">
        <div class="about-main">
            <div class="about-top wow fadeInDown" data-wow-delay="0.3s">
                <h2>News</h2>
                <span class="heading-line"> </span>
                <p>Nemo enim ipsam voluptatem quia.</p>
            </div>
            <div class="about-bottom">
                <div class="col-md-6 about-left wow fadeInLeft" data-wow-delay="0.3s">
                    <h4>Our Success</h4>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium totam.</p>
                    <div id="inner-content-div" role="tablist">

                        @if($data['about'])
                        @php($counter=0)
                        @foreach($data['about'] as $about)
                            @php($counter++)
                            <div role="presentation" class="about-grid {!! $counter==1?'active':'' !!}" style="height: 60px;">
                                <div class="about-icon">
                                    <a href="#profile{{ $counter }}" aria-controls="profile{{ $counter }}" role="tab" data-toggle="tab"> <span class="{{ $about->image }}" style="font-size: 40px; padding: 10px;"> </span> </a>
                                </div>
                                <div class="about-text">
                                    <h5>{{ $about->title }}</h5>
                                    <p>{!! str_limit($about->description,150) !!}</p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>

                            @endforeach
                        @endif
                    </div>

                </div>
                <div class="col-md-6 about-right wow fadeInRight" data-wow-delay="0.3s">



                    <div class="tab-content">

                        @if($data['about'])
                            @php($counter=0)
                            @foreach($data['about'] as $about)
                                @php($counter++)

                                <div style="text-align: center" role="tabpanel" class="tab-pane {!! $counter==1?'active':'' !!}" id="profile{{ $counter }}">
                                    <a href="#profile{{ $counter }}" aria-controls="profile{{ $counter }}" role="tab" data-toggle="tab"> <span class="{{ $about->image }}" style="font-size: 40px; padding: 20px;"> </span> </a>
                                    <p>{!! $about->description !!}</p>
                                </div>
                                {{--<div role="tabpanel" class="tab-pane {!! $counter==1?'active':'' !!}" id="profile{{ $counter }}"><img src="{{ asset('images/about-us/'.$about->image) }}" style="height: 400px;" class="img-responsive" alt=""></div>--}}
                                {{--<div role="tabpanel" class="tab-pane {!! $counter==1?'active':'' !!}" id="profile{{ $counter }}">{{ $about->title }}</div>--}}

                            @endforeach
                        @endif
                    </div>


                </div>


                <div>

                    <!-- Nav tabs -->


                    <!-- Tab panes -->


                </div>



                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $('#inner-content-div').slimScroll({
            height: '300px'
        });
    });
</script>