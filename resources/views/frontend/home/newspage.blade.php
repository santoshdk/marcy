@extends('frontend.common.layout')

@section('content')

    @include('frontend.common.header')

    <!--header end here-->
    <!--services start here-->

    @include('frontend.home.news')

    <!--gallery-->
    <script src="{{ asset('frontend/js/easyResponsiveTabs.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#horizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true   // 100% fit in a container
            });
        });

    </script>
    <!--advantages start here-->

    @include('frontend.home.feedback')


    @include('frontend.common.footer')

    <!--copy rights end here-->

@endsection