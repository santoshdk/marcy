<?php

use Illuminate\Database\Seeder;

class PrincipleMessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('principle')->insert([
            'title'               =>     'Sir',
            'slug'               =>     'principle-name',
            'image'                =>    'filename',
            'message'          =>     'hello',
            'status' => 1,
        ]);
    }
}
