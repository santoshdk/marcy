<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => 'mercy@gmail.com',
            'password' => bcrypt('mercy@12'),
            'name' => 'Admin',
            'status' => true,
            'type' => 'admin',
            'slug'=>'admin'
        ]);
    }
}
