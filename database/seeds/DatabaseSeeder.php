<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(PageSeeder::class);
        $this->call(MenuTableSeeder::class);
        $this->call(aboutusSeeder::class);
        $this->call(ProfileSeeder::class);
        $this->call(PrincipleMessageSeeder::class);
    }
}
