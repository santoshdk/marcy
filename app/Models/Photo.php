<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $table = 'photo';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['title', 'image', 'gallery_id', 'description', 'status'];




}
