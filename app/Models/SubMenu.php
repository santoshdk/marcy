<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubMenu extends Model
{
    protected $table = 'sub_menu';

    protected $fillable = ['created_by', 'updated_by', 'title', 'slug', 'link', 'menu_id', 'target', 'status'];



}
