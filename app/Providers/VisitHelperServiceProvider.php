<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App;

class VisitHelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('visithelper', function()
        {
            return new \App\HelperClasses\VisitHelper;
        });

    }
}
