<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class VisitHelperFacade extends Facade{
    protected static function getFacadeAccessor() { return 'visithelper'; }
}