<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Menu\Add;
use App\Models\Menu;
use App\Models\Page;
use Illuminate\Http\Request;

class MenuController extends AdminBaseController
{

    public $base_route = 'admin.menu';
    public $view_path = 'admin/menu';
    public $view_title = 'Menu Manager';
    public $trans_path = 'santosh';

    public function index()
    {
        $data = [];
        $data['menu'] = Menu::select()->where('parent_menu','=',0)->paginate(10);
        $a =$data['sub-menu'] = Menu::select()->where('parent_menu','!=',0)->get();
        $data['page'] = Page::select('title','id')->get();

        return view(parent::loadDefaultVars($this->view_path.'.list'),compact('data'));
    }

    public function add()
    {

        $data['parent_menu'] = Menu::select('title','id')->where('parent_menu',0)->get();
        $data['page'] = Page::select()->get();
        return view(parent::loadDefaultVars($this->view_path.'.add'),compact('data'));
        
    }

    public function store(Add $request)
    {

        $parent_menu = $request->get('dropdown')=='0'?'0':$request->get('parent_menu');

        Menu::create([
            'title' => $request->get('title'),
            'slug' => str_slug($request->get('slug')),
            'link' => $request->get('link'),
            'page_id' => $request->get('page_id'),
            'parent_menu' => $parent_menu,
            'target' => $request->get('target'),
            'status' => $request->get('status'),
        ]);
        return redirect()->route($this->base_route.'.index');

    }

    public function edit($id)
    {

        $data = [];
        $data['row'] = Menu::find($id);
        $data['page'] = Page::select()->get();
        $data['parent_menu'] = Menu::select('title','id')->where('parent_menu',0)->get();

        return View(parent::loadDefaultVars($this->view_path.'.edit'),compact('data'));

    }

    public function update(Request $request,$id)
    {

        if (!$data = Menu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $parent_menu = $request->get('dropdown')=='0'?'0':$request->get('parent_menu');

        $data->update([
            'title' => $request->get('title'),
            'link' => $request->get('link'),
            'parent_menu' => $parent_menu,
            'page_id' => $request->get('page_id'),
            'target' => $request->get('target'),
            'status' => $request->get('status'),
        ]);

        return redirect()->route($this->base_route.'.index');

    }

    public function delete($id)
    {

        if (!$data = Menu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        foreach ($data->children as $child){
            $childata = Menu::find($child->id);
            $childata->delete();
        }

        $data->delete();

        return redirect()->route($this->base_route.'.index');

    }

}
